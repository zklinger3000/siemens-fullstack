# Zack Klinger's Submission for Siemens Fullstack SPA

## Setup Instructions

### Backend API

#### 1. Clone Repo

```
git clone https://zklinger3000@bitbucket.org/zklinger3000/siemens-fullstack.git
```

#### 2. NPM Install

```
cd siemens-fullstack/backend-api
npm install
```

#### 3. Start Server

```
npm start
```

> You should now see the backend Expressjs server running.

```
Server listening on port 8050
```

### React App

#### 1. Open a new terminal window

```
cd <path-to-code>/siemens-fullstack/frontend
```

#### 2. NPM Install

```
npm install
```

#### 3. Start Frontend Dev Server

```
npm start
```

> Your browser should automatically open a new tab at:  
> ```http://localhost:3000```
