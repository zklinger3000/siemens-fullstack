"use strict";
import templatesController from '../controllers/templates';

//================
// Routes
//================
const routes = (app) => {

  // extendedTemplate.json
  app.get('/api/extended-template', templatesController.extendedTemplate);

  // templates.json
  app.get('/api/templates', templatesController.templates);

  // Test route
  app.get('/api/test', (req, res) => {
    res.json({ message: 'no authorization required' });
  });

};

export default routes;
