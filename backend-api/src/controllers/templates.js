"use strict";
import fs from 'fs';
import path from 'path';

// Read extendedTemplate.json
const pathExtendedTemplate = path.join(path.resolve(), '/src/data/extendedTemplate.json');
let rawExtendedTemplate = fs.readFileSync(pathExtendedTemplate);
let extendedTemplate;
try {
  extendedTemplate = JSON.parse(rawExtendedTemplate);
} catch (err) {
  console.error(err);
}
// Read templates.json
const pathTemplates = path.join(path.resolve(), '/src/data/templates.json');
let rawTemplates = fs.readFileSync(pathTemplates);
let templates;
try {
  templates = JSON.parse(rawTemplates);
} catch (err) {
  console.error(err);
}

//=============
// Controller
//=============
const templatesController = {
  extendedTemplate: (req, res) => {
    res.send(extendedTemplate);
  },
  templates: (req, res) => {
    res.send(templates);
  }
};

export default templatesController;
