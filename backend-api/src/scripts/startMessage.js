"use strict";
import chalk from 'chalk';

console.log(chalk.blueBright('arch:'), process.arch, chalk.blueBright('node:'), process.version);

console.log(chalk.green('Starting app in'),
  chalk.blue(process.env.NODE_ENV || 'local'),
  chalk.green('mode...'));
