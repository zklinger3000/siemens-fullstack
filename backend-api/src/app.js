"use strict";
/* eslint-disable no-console */
import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';
import routes from './routes';

// Initialize Express app
const app = express();

// Middleware for logging
app.use(morgan('combined'));

// Middleware parses incoming request payloads into JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: '*/*' }));

// Could close off our API to rest of world on next line in cors parameters
const corsOptions = { // eslint-disable-line no-unused-vars
  origin: 'http://localhost:3000',
  allowedHeaders: ['Content-Type', 'Access-Control-Allow-Origin'],
  credentials: true,
  preflightContinue: true
};
// CORS Middleware for handling requests coming from different IPs/ports
app.use(cors(corsOptions));

// Pass in our app to make it available to all routes
routes(app);

// Global error logging if any errors make it past our route controllers
// app.use((err, req, res, next) => {
//   res.status(422).send({ error: err.message });
// });

export default app;
