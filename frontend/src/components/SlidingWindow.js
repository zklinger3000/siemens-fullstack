import React, { useState, useEffect } from 'react';

const SlidingWindow = ({ templates, onClickHandler, currentId, step }) => {
  let [sliced, setSliced] = useState(templates.slice(0, step));
  let [slicedIndex, setSlicedIndex] = useState(0);

  useEffect(() => {
    setSliced(templates.slice(slicedIndex, slicedIndex + step));
  }, [slicedIndex, templates, step])

  const onNavClick = (event, step) => {
    if (step > 0) {
      if (templates.length >= slicedIndex + step) {
        setSlicedIndex(slicedIndex + step);
      }
    } else {
      if (slicedIndex + step >= 0) {
        setSlicedIndex(slicedIndex + step);
      }
    }
  };

  return (
    <div className="thumbnails">
      <div className="group">
        { sliced.map((template, index) => {
            return (
              <a href="#" className={template.id === currentId ? 'active' : ''} key={index} title={template.id} onClick={onClickHandler} id={template.id}>
                <img src={`images/thumbnails/${template.thumbnail}`} alt={template.thumbnail.slice(0, (template.thumbnail.length - 4))} width="145" height="121" />
                <span>{template.id}</span>
              </a>
            );
        }) }
        <span className={`previous ${slicedIndex === 0 ? 'disabled' : ''}`} title="Previous" onClick={(e) => onNavClick(e, (-1 * step))}>Previous</span>
        <a href="#" className={`next ${templates.length >= slicedIndex + step ? '' : 'disabled'}`}  title="Next" onClick={(e) => onNavClick(e, step)}>Next</a>
      </div>
    </div>
  );
};

export default SlidingWindow;
