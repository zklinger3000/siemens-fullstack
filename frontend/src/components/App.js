import React, { useState, useEffect } from 'react';
import api from '../apis/backend';
import MainWindow from '../components/MainWindow';
import SlidingWindow from '../components/SlidingWindow';

const App = () => {
  let [currentTemplate, setCurrentTemplate] = useState({});
  const [templates, setTemplates] = useState([]);
  let [currentId, setCurrentId] = useState();

  useEffect(() => {
    fetchTemplates();
  }, []);
  
  const fetchTemplates = async () => {
    const response = await api.get('/templates');
    if (response.status === 200) {
      setTemplates(response.data);
      setCurrentTemplate(response.data[0]);
      setCurrentId(response.data[0].id);
    }
  }

  const onTemplateSelect = event => {
    const { id } = event.target.offsetParent;
    setCurrentId(id);
    // Update current template by id
    setCurrentTemplate(templates.filter(template => {
      return template.id === id;
    })[0]);
  }

  return (
    <div id="container">
      <header>
      Code Development Project
      </header>
      <div id="main" role="main">
        <MainWindow template={currentTemplate}/>
        <SlidingWindow templates={templates} currentId={currentId} onClickHandler={onTemplateSelect} step={4}/>
      </div>
      <footer>
        <a href="instructions.pdf">Download PDF Instructions</a>
      </footer>
    </div> 
  );
};

export default App;
